<?php

	// echo json_encode([
	// 	"a" => "b",
	// 	"c" => [
	// 		"d" => "e"
	// 	]
	// ]);
	ini_set('memory_limit', '256M'); // or you could use 1G
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	$db = mysqli_connect('localhost','root','','db_kai')
			or die('Error connecting to MySQL server.');

	$tanggal = '2017-08-17';

	$stasiun = [];

	$stasiun_query = "SELECT * FROM stasiun";
	$stasiun_result = mysqli_query($db, $stasiun_query);
	while ($row = mysqli_fetch_array($stasiun_result)) {
		$stasiun += [$row["nama_stasiun"] => $row["id_st"] ];
	}

	$asal = $stasiun[$_GET["asal"]];

	$tujuan = $stasiun[$_GET["tujuan"]];

	$kereta_query = "SELECT * FROM (SELECT COUNT(id_krt) as count, nama, id_krt, id_st, tiba FROM kereta WHERE tanggal = '$tanggal' AND (id_st = $asal OR id_st = $tujuan)) temp WHERE count = 2";  
	mysqli_query($db, $kereta_query) or die('Error querying database kereta');
	$kereta = mysqli_query($db, $kereta_query);

	//variabel untuk menyimpan ketersediaan kursi
	$sedia = [];	
	$mboh = array();

	while ($row = mysqli_fetch_array($kereta)) {
		// echo '<br />'.$row["id_krt"];

		$id_krt = $row["id_krt"];

		$path_query = "SELECT id_st FROM path_kereta WHERE id_krt = $id_krt";
		
		$path = mysqli_query($db, $path_query);
		
		// echo '<br/>'.mysqli_num_rows($path)." total rows";

		$stasiun = [];
		$kursi = [];

		$flag = False;

		while ($r = mysqli_fetch_array($path)) {
			// echo '<br/>  &nbsp &nbsp'.$r["id_st"];

			$id_st = $r["id_st"];
			if ($id_st == $asal)
				$flag = !$flag;
			if ($id_st == $tujuan)
				$flag = !$flag;
			if ($flag) {
				// echo '<br />'.$id_st;
				array_push($stasiun, $id_st);
				$kursi_query = "SELECT nomor FROM kursi WHERE id_krt = $id_krt AND id_st = $id_st";
				$kursi_result = mysqli_query($db, $kursi_query);
				$nomor = [];
				while($i = mysqli_fetch_array($kursi_result)) {
					array_push($nomor,$i["nomor"]);
				}

				$kursi += [ $r["id_st"] => $nomor];
			}
		}

		foreach ($kursi as $key1 => $no) {
			// echo '<br/>'.$key.' => ';

			foreach ($no as $value) {
				// echo '<br/> &nbsp &nbsp'.$value;
				if ($key1 == $asal) {
					$mkursi = array(array($asal,$value));
					array_push($sedia,$mkursi);
				} else {
					foreach ($sedia as $i => $s) {
						$cek_ada = False;
						foreach ($s as $k => $v) {
							if ($v[1] == $value) {
								$cek_ada = True;
							}
						}
						$temp = $sedia[$i];
						if (!$cek_ada && count($sedia[$i]) <= 1 && $key1 != $temp[count($temp)-1][0]) {
							array_push($sedia,$temp);
							array_push($sedia[$i],array($key1,$value));
						}
					}
				}
			}
		}

		$data = [];
		$data_query = "SELECT * FROM kereta INNER JOIN stasiun ON kereta.id_st = stasiun.id_st WHERE id_krt = $id_krt";
		$data_result = mysqli_query($db, $data_query);
		while ($r = mysqli_fetch_array($data_result)) {
			$data += [$r["id_st"] => array("nama" => $r["nama_stasiun"], "tiba" => $r["tiba"], "kota")];
		}
		$count_page_2 = 0;
		$count_page_1 = 0;
		
		$result = [];
		$kereta_satu = [];
		$kereta_dua = [];



		$waktu_tiba_query = "SELECT tiba FROM kereta WHERE id_krt = $id_krt AND id_st = $tujuan";
		$waktu_tiba_result = mysqli_query($db, $waktu_tiba_query);
		$waktu_tiba;
		while ($r = mysqli_fetch_array($waktu_tiba_result)) {
			$waktu_tiba = $r["tiba"];
		}
		// echo '&nbsp'.$waktu_tiba;

		foreach ($sedia as $value) {
			if ($count_page_1 < 5 && count($value) == 1) {
				// echo '<br/>'.$row["nama"].' ';
				foreach ($value as $v) {
					// echo '&nbsp'.$data[$v[0]]["nama"].'&nbsp'.$data[$v[0]]["tiba"].'&nbsp '.$v[1]; 
				}
				$count_page_1++;

				array_push($kereta_satu, array("stasiun_awal" => $data[$v[0]]["nama"], "berangkat" => $data[$v[0]]["tiba"] , "kursi" => $v[1], "tiba" => $waktu_tiba));
			}

			if ($count_page_2 < 5 && count($value) == 2) {
				// echo '<br/>'.$row["nama"].' ';
				$rute = [];
				$count = 0;
				foreach ($value as $v) {
					// echo '&nbsp'.$data[$v[0]]["nama"].'&nbsp'.$data[$v[0]]["tiba"].'&nbsp '.$v[1];
					if ($count == 0) {
						$rute += [ "stasiun_awal" => $data[$v[0]]["nama"] ];
						$rute += [ "berangkat" => $data[$v[0]]["tiba"] ];
						$rute += [ "kursi_pertama" => $v[1] ];
					} else {
						$rute += [ "stasiun_transit" => $data[$v[0]]["nama"] ];
						$rute += [ "waktu_transit" => $data[$v[0]]["tiba"] ];
						$rute += [ "kursi_kedua" => $v[1] ];
						$rute += [ "tiba" => $waktu_tiba ];
					}
					$count++;
				}
				$count_page_2++;
				array_push($kereta_dua, $rute);
			}			
		}
		$result += [ "nama" => $row["nama"] ];
		$result += [ "1" => $kereta_satu ];
		$result += [ "2" => $kereta_dua ];

		array_push($mboh, $result);

		//Percobaan cara penggunaan
		// echo '<br/>';
		// echo '<br/>';
		// echo '<br/>';
		// echo '<br/>';
		// echo '<br/>';
		// echo '<br/> CARA PENGGUNAAN';

		// echo '<br/> nama kereta : '.$result["nama"];
		// echo '<br/> Direct : ';
		// foreach ($result["1"] as $value) {
		// 	echo '<br/> '.$value["stasiun_awal"].' '.$value["berangkat"].' '.$value["tiba"].' '.$value["kursi"];	
		// }

		// echo '<br/> Dengan Transit : ';
		// foreach ($result["2"] as $value) {
		// 	echo '<br/> '.$value["stasiun_awal"].' '.$value["berangkat"].' '.$value["kursi_pertama"].' '.$value["stasiun_transit"].' '.$value["waktu_transit"].' '.$value["kursi_kedua"].' '.$value["tiba"];	
		// }
		
		// echo '<br/> nama kereta : '.$result["nama"];
		// echo '<br/> Direct : ';
		// foreach ($result["1"] as $value) {
		// 	echo '<br/> '.$value["stasiun_awal"].' '.$value["berangkat"].' '.$value["tiba"].' '.$value["kursi"];	
		// }

		// echo '<br/> Dengan Transit : ';
		// foreach ($result["2"] as $value) {
		// 	echo '<br/> '.$value["stasiun_awal"].' '.$value["berangkat"].' '.$value["kursi_pertama"].' '.$value["stasiun_transit"].' '.$value["waktu_transit"].' '.$value["kursi_kedua"].' '.$value["tiba"];	
		// }
		
	}

	function getBulan($tanggal_bulan) {
		switch ($tanggal_bulan) {
			case 1 :
			$tanggal_bulan = "Januari";
			break;
			case 2 :
			$tanggal_bulan = "Februari";
			break;
			case 3 :
			$tanggal_bulan = "Maret";
			break;
			case 4 :
			$tanggal_bulan = "April";
			break;
			case 5 :
			$tanggal_bulan = "Mei";
			break;
			case 6 :
			$tanggal_bulan = "Juni";
			break;
			case 7 :
			$tanggal_bulan = "Juli";
			break;
			case 8 :
			$tanggal_bulan = "Agustus";
			break;
			case 9 :
			$tanggal_bulan = "Agustus";
			break;
			case 10 :
			$tanggal_bulan = "September";
			break;
			case 11 :
			$tanggal_bulan = "Oktober";
			break;
			case 12 :
			$tanggal_bulan = "Desember";
			break;
		}
		return $tanggal_bulan;
	}

	include 'result.php';

?>
