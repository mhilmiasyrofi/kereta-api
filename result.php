<!DOCTYPE html>
<html>
<head>
	<title>Hasil pencarian</title>
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
	
</head>
<body style="padding-top:3cm">

		<div class="container">

		<nav>
				<div class="nav-wrapper blue valign-wrapper">
					<div class="col s12" style="padding-left:1cm">
						<h5><?php echo $_GET["asal"]?> &nbsp&nbsp&nbsp ->&nbsp&nbsp&nbsp <?php echo $_GET["tujuan"]?></h5>
					</div>
				</div>
		</nav>
		

		<div class="row" style="margin-top:1cm">
		    <div class="col m12">
		      <div class="card">
		        <div class="card-content; padding:0px">
		          <table class="bordered centered">
								
								<thead>
									<tr>
											<th>Kereta Api</th>
											<th>Berangkat</th>
											<th>Datang</th>
											<th>Durasi</th>
											<th>Harga</th>
											<th>
												<img src="http://1.bp.blogspot.com/-_GDtIf8Q2SA/UP1ahk4u2pI/AAAAAAAACZ0/DssvI_FlS94/s200/PT+Kereta+Api+%2528Persero%2529-lowongan-bumn.com.jpg" style="height:0.8cm"/>
											</th>
									</tr>
								</thead>
				
								<tbody>

									<?php foreach ($mboh as $mboh2) : 
										$nama = $mboh2["nama"];
											foreach ($mboh2["1"] as $mboh3) : 
												$waktu = $mboh3["berangkat"];
												$waktu_array = explode(" ", $waktu);
												$jam = $waktu_array[1];
												$jam_array = explode(":", $jam);
												$jam_jam = $jam_array[0];
												$jam_menit = $jam_array[1];
												$tanggal = $waktu_array[0];
												$tanggal_array = explode("-", $waktu_array[0]);
												$tanggal_tanggal = $tanggal_array[2];
												$tanggal_bulan = getBulan($tanggal_array[1]);
												$tanggal_tahun = $tanggal_array[0];
												$tanggal_tampil = $tanggal_tanggal." ".$tanggal_bulan." ".$tanggal_tahun;
												$datetime = date_create($waktu);

												$waktu2 = $mboh3["tiba"];
												$waktu_array2 = explode(" ", $waktu2);
												$jam2 = $waktu_array2[1];
												$jam_array2 = explode(":", $jam2);
												$jam_jam2 = $jam_array2[0];
												$jam_menit2 = $jam_array2[1];
												$tanggal2 = $waktu_array2[0];
												$tanggal_array2 = explode("-", $waktu_array2[0]);
												$tanggal_tanggal2 = $tanggal_array2[2];
												$tanggal_bulan2 = getBulan($tanggal_array2[1]);
												$tanggal_tahun2 = $tanggal_array2[0];
												$tanggal_tampil2 = $tanggal_tanggal2." ".$tanggal_bulan2." ".$tanggal_tahun2;
												$datetime2 = date_create($waktu2);

												$different = date_diff($datetime, $datetime2);
												$durasi = $different->format('%hj %im');

											?>
												<tr>
													<td><?=$nama?></td>
													<td><?=$jam_jam?>.<?=$jam_menit?><br><small><?=$tanggal_tampil?></small></td>
													<td><?=$jam_jam2?>.<?=$jam_menit2?><br><small><?=$tanggal_tampil2?></small></td>
													<td><?=$durasi?><br><small>Direct</small></td>
													<td>Rp 445.000<br><small>Subclass A</small></td>
													<td><b><a class="btn-flat yellow">Pesan</a></b></td>
												</tr>
											<?php endforeach; ?>
											<?php foreach ($mboh2["2"] as $mboh3) : 
												
												$waktu = $mboh3["berangkat"];
												$waktu_array = explode(" ", $waktu);
												$jam = $waktu_array[1];
												$jam_array = explode(":", $jam);
												$jam_jam = $jam_array[0];
												$jam_menit = $jam_array[1];
												$tanggal = $waktu_array[0];
												$tanggal_array = explode("-", $waktu_array[0]);
												$tanggal_tanggal = $tanggal_array[2];
												$tanggal_bulan = getBulan($tanggal_array[1]);
												$tanggal_tahun = $tanggal_array[0];
												$tanggal_tampil = $tanggal_tanggal." ".$tanggal_bulan." ".$tanggal_tahun;
												$datetime = date_create($waktu);

												$waktu2 = $mboh3["tiba"];
												$waktu_array2 = explode(" ", $waktu2);
												$jam2 = $waktu_array2[1];
												$jam_array2 = explode(":", $jam2);
												$jam_jam2 = $jam_array2[0];
												$jam_menit2 = $jam_array2[1];
												$tanggal2 = $waktu_array2[0];
												$tanggal_array2 = explode("-", $waktu_array2[0]);
												$tanggal_tanggal2 = $tanggal_array2[2];
												$tanggal_bulan2 = getBulan($tanggal_array2[1]);
												$tanggal_tahun2 = $tanggal_array2[0];
												$tanggal_tampil2 = $tanggal_tanggal2." ".$tanggal_bulan2." ".$tanggal_tahun2;
												$datetime2 = date_create($waktu2);

												$different = date_diff($datetime, $datetime2);
												$durasi = $different->format('%hj %im');


												// [berangkat] => 2017-08-17 19:30:00
												// [kursi_pertama] => 1D
												// [stasiun_transit] => Tasikmalaya
												// [waktu_transit] => 2017-08-17 22:14:00
												// [kursi_kedua] => 1A
												// [tiba] => 2017-08-18 03:25:00
												$asal = $_GET["asal"];
												$tujuan = $_GET["tujuan"];
												$transit = $mboh3["stasiun_transit"];
												$kursi1 = $mboh3["kursi_pertama"];
												$kursi2 = $mboh3["kursi_kedua"];
												$transit_time = date_create($mboh3["waktu_transit"]);
												$transit_jam = $transit_time->format('h:m');
												$transit_tanggal = $transit_time->format('d') ." ". getBulan($transit_time->format('m')) ." ". $transit_time->format('Y');
												
												?>
												<tr>
												<td><?=$nama?></td>
												<td><?=$jam_jam?>.<?=$jam_menit?><br><small><?=$tanggal_tampil?></small></td>
												<td><?=$jam_jam2?>.<?=$jam_menit2?><br><small><?=$tanggal_tampil2?></small></td>
												<td><?=$durasi?><br><small>Direct</small></td>
												<td>Rp 445.000<br><small>Subclass A</small></td>
												<td><b><a class="btn-flat yellow">Pesan</a></b></td>
												</tr>
									<tr>
										<td colspan="6" class="center-align" valign="middle" style="padding-bottom: 80px;">
											<div>
												<div class="col m2">
												</div>
												<div class="col m2">
													<span class="grey-text"><?=$asal?></span><br>
													o<br>
													<?=$jam_jam?>:<?=$jam_menit?> <br>
													<small><?=$tanggal_tampil?></small>
												</div>
												<div class="col m1 grey-text">
														<br>
														<hr>
														Gerbong 2 <br>
														<?=$kursi1?>
												</div>
												<div class="col m2">
													<span class="grey-text"><?=$transit?></span><br>
													o<br>
													<?=$transit_jam?> <br>
													<small><?=$transit_tanggal?></small>
												</div>
												<div class="col m1 grey-text">
														<br>
														<hr>
														Gerbong 1 <br>
														<?=$kursi2?>
												</div>
												<div class="col m2">
													<span class="grey-text"><?=$tujuan?></span><br>
													o<br>
													<?=$jam_jam2?>:<?=$jam_menit2?> <br>
													<small><?=$tanggal_tampil2?></small>
												</div>
												<div class="col m2">
												</div>																		
											</div>
										</td>
									</tr>


											<?php endforeach; ?>
									<?php endforeach; ?>

								</tbody>
							</table>
		        </div>
		      </div>
		    </div>
	  	</div>
	</div>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>

</body>
</html>